using DrWatson
@quickactivate "WebApp"
#widget(; label="")

using WebIO , Interact
#from https://juliaobserver.com/packages/Interact
#For example,
px=widget(0:0.01:.3, label="px")
py=widget(0:0.01:.3, label="py")
plane = widget(Dict("x=0" => 1, "y=0" => 2), label="Section plane");
#Then you can use map over the widgets to create new interactive output that depends on these widgets!
#interactive_plot = map(plotsos, px, py, plane)
#And put them all together into a nice little layout:
# stack vertically
vbox(
    hbox(px, py, plane), # stack horizontally
    interactive_plot)
